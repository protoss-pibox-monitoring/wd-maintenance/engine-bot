package com.protoss.pm.bot.util;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ratthanan-w on 8/5/2562.
 */
@Component
public class ConstantVariable {


    public static String CODE_USE_FOR_SEPERATE_WORD = "code.seperate";
    public static String ALARM_ACTIVE = "1";   /* 1 = enable , 0 = disable */
    public static String WEBHOOK_FIRST_RESPONSE_TEXT = "HELLO, WORLD";

}
