package com.protoss.pm.bot.service;

import com.protoss.pm.bot.entity.GroupLine;
import com.protoss.pm.bot.entity.LineLogMessage;
import com.protoss.pm.bot.entity.ProjectGroup;
import com.protoss.pm.bot.repository.GroupLineRepository;
import com.protoss.pm.bot.repository.LineLogMessageRepository;
import com.protoss.pm.bot.repository.ProjectGroupRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class GroupLineServiceImp implements GroupLineService {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private GroupLineRepository groupLineRepository;
    
    @Autowired
    private ProjectGroupRepository projectGroupRepository;


    @Transactional
    @Override
    public Long insertGroupLine(GroupLine obj) {
        try{
            List<GroupLine> groupLines = groupLineRepository.findByRealId(obj.getRealId());

            if(groupLines.size() == 0){
                groupLineRepository.saveAndFlush(obj);
            }else{
                //groupLineRepository.deleteAll(groupLines);
                //groupLineRepository.flush();
                //groupLineRepository.saveAndFlush(obj);
            	groupLines.get(0).setDisplayName(obj.getDisplayName());
                LOGGER.info(" ===- [insertGroupLine] Msg : [Duplicate Group Id]");
                
                ProjectGroup projectGroup = projectGroupRepository.findFirstByRealId(groupLines.get(0).getRealId());
                projectGroup.setDisplayName(obj.getDisplayName());
            }

            return obj.getId();
        }catch (Exception e){
            LOGGER.error(" ===- Exception[insertGroupLine] Msg : [{}]",e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }


	@Override
	public GroupLine findFirstByCode(String code) {
		// TODO Auto-generated method stub
		
		return groupLineRepository.findFirstByCode(code);
	}


	@Override
	public GroupLine findFirstByRealId(String realId) {
		// TODO Auto-generated method stub
		return groupLineRepository.findFirstByRealId(realId);
	}


	@Override
	public void updateGroupLine(GroupLine obj) {
		// TODO Auto-generated method stub
		groupLineRepository.saveAndFlush(obj);
	}
}
