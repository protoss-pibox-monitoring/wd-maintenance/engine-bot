package com.protoss.pm.bot.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

public interface SpecialService {

    public List<Map<String,Object>> generateKeyword();
    public int findHighPriorityMessage(String str);
    public Map findWordMatches(String text, List<Map<String,Object>> list);
    public String seperateNameGroupLine(String str);
    public Map<String,Object> findMessageResponse(String str,String groupId);


}
