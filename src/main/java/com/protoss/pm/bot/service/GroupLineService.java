package com.protoss.pm.bot.service;

import org.springframework.data.repository.query.Param;

import com.protoss.pm.bot.entity.GroupLine;
import com.protoss.pm.bot.entity.LineLogMessage;

public interface GroupLineService {



    Long insertGroupLine(GroupLine obj);
    void updateGroupLine(GroupLine obj);
    GroupLine findFirstByCode(@Param("code")String code);
    GroupLine findFirstByRealId(@Param("realId")String realId);

}
