package com.protoss.pm.bot.service;

import com.protoss.pm.bot.controller.LineBotController;
import com.protoss.pm.bot.entity.GroupLine;
import com.protoss.pm.bot.entity.LineHistoryLogMessage;
import com.protoss.pm.bot.entity.LineLogMessage;
import com.protoss.pm.bot.entity.MasterDataDetail;
import com.protoss.pm.bot.repository.GroupLineRepository;
import com.protoss.pm.bot.repository.LineHistoryLogMessageRepository;
import com.protoss.pm.bot.repository.LineLogMessageRepository;
import com.protoss.pm.bot.repository.MasterDataDetailRepository;

import lombok.extern.slf4j.Slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class LineHistoryLogMessageServiceImp implements LineHistoryLogMessageService {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private LineHistoryLogMessageRepository lineHistoryLogMessageRepository;

    @Autowired
    private GroupLineRepository groupLineRepository;


    @Transactional
    @Override
    public Long insertLineHistoryLogMessage(LineHistoryLogMessage obj) {
        try{
            List<GroupLine> groupLineList = groupLineRepository.findByRealId(obj.getGroupLine());
            if(groupLineList.size() > 0){
                obj.setGroupLine(groupLineList.get(0).getDisplayName());
            }
            lineHistoryLogMessageRepository.saveAndFlush(obj);
            return obj.getId();
        }catch (Exception e){
                LOGGER.error(" ===- Exception[insertLineLogMessage] Msg : [{}]",e.getMessage());
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
        }


    }


	@Override
	public Boolean findLastLineHistoryLogMessage(String groupLine, String sender) {
		Boolean hasData = false;
		try{ 
			groupLine = groupLineRepository.findFirstByRealId(groupLine).getDisplayName(); 
		}catch (Exception e){ }
		
		
		log.info(" ===- groupLine= {} -===",groupLine);
		log.info(" ===- sender= {} -===",sender);
		LineHistoryLogMessage lineHistoryLogMessage = lineHistoryLogMessageRepository.findTopByGroupLineAndSenderOrderByCreatedDateDesc(groupLine, sender);
		log.info(" ===- lineHistoryLogMessage= {} -===",lineHistoryLogMessage);
		if(lineHistoryLogMessage !=null){
			
			Long diffTime = System.currentTimeMillis() - lineHistoryLogMessage.getCreatedDate().getTime() ;
			
			log.info(" ===- diffTime= {} -===",diffTime);
			Double diffMinute = Double.valueOf(String.valueOf(diffTime)) / (1000 * 60 * 5);
			log.info(" ===- diffMinute= {} -===",diffMinute);
			if(diffMinute.doubleValue() < 1.0){
				hasData = true;
			}
		}
		// TODO Auto-generated method stub
		return hasData;
	}
}
