package com.protoss.pm.bot.service;

import java.util.List;

import com.protoss.pm.bot.entity.LineLogMessage;
import com.protoss.pm.bot.entity.MasterDataDetail;

public interface LineLogMessageService {

    Long insertLineLogMessage(LineLogMessage obj);

}
