package com.protoss.pm.bot.service;

import java.util.List;

import com.protoss.pm.bot.entity.MasterDataDetail;

public interface AppLineBotDataService {

    List<MasterDataDetail> masterDatakey(Long id, String code);
    boolean checkTextMatches(String str);
    String checkText(String str);
    String checkPriorityMessage(String str);

}
