package com.protoss.pm.bot.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.protoss.pm.bot.entity.LineHistoryLogMessage;
import com.protoss.pm.bot.entity.LineLogMessage;
import com.protoss.pm.bot.entity.MasterDataDetail;

public interface LineHistoryLogMessageService {

    Long insertLineHistoryLogMessage(LineHistoryLogMessage obj);
    Boolean findLastLineHistoryLogMessage(String groupLine,String sender);

}
