package com.protoss.pm.bot.service;

import org.springframework.stereotype.Service;

import com.protoss.pm.bot.listener.NetPieListener;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NetPieServiceImpl implements NetPieService{
	
	NetPieListener netPieListener;

	public NetPieServiceImpl() {
		netPieListener = new NetPieListener();
	}
	
	public void chat(String gearName,String message){
		netPieListener.chat(gearName, message);
	}
	
	public void publish(String message){
		netPieListener.publish(message);
	}

}
