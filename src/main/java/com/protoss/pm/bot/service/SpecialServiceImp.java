// package com.protoss.pm.bot.service;

// import java.util.ArrayList;
// import java.util.HashMap;
// import java.util.List;
// import java.util.Map;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Service;
// import org.springframework.web.client.RestTemplate;

// import com.protoss.pm.bot.entity.IssueMessageWord;
// import com.protoss.pm.bot.entity.MasterData;
// import com.protoss.pm.bot.entity.MasterDataDetail;
// import com.protoss.pm.bot.entity.Priority;
// import com.protoss.pm.bot.repository.IssueMessageWordRepository;
// import com.protoss.pm.bot.repository.MasterDataRepository;
// import com.protoss.pm.bot.repository.PriorityRepository;
// import com.protoss.pm.bot.util.ConstantVariable;

// @Service
// public class SpecialServiceImp implements SpecialService {
//     private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

//     protected RestTemplate restTemplate;

//     @Autowired
//     PriorityRepository priorityRepository;

//     @Autowired
//     IssueMessageWordRepository issueMessageWordRepository;

//     @Autowired
//     MasterDataRepository masterDataRepository;


//     @Override
//     public List<Map<String, Object>> generateKeyword() {
//         List<Map<String,Object>> listResult = new ArrayList<>();
//         LOGGER.info(" ===- IN generateKeyword -===");

//         List<IssueMessageWord> matchesWordList = issueMessageWordRepository.findAll();
//         LOGGER.info(" ===-  generateKeyword size list [{}]-===",matchesWordList.size());
//         Map<String,Object> mapData = null;
//         for(IssueMessageWord data : matchesWordList){
//             mapData = new HashMap<>();
//             mapData.put("inputText",data.getInputText());
//             mapData.put("outputText",data.getOutputText());
//             listResult.add(mapData);

//         }

//         LOGGER.info(" ===- OUT generateKeyword -===");
//         return listResult;
//     }

//     @Override
//     public int findHighPriorityMessage(String str) {
//         LOGGER.info(" ===- IN findHighPriority -===");
//         int result = 99;
//         List<Priority> priorityList = priorityRepository.findAll();

//         for(Priority data : priorityList){
//             if(str.trim().startsWith(data.getHasText()) == true && "Y".equals(data.getFlagActive())){
//                 result =  (data.getValue() < result ? data.getValue() : result);

//             }

//         }
//         LOGGER.info(" ===- findHighPriority priority[{}] -===",result);

//         LOGGER.info(" ===- OUT findHighPriority -===");

//         return result;    }

//     @Override
//     public Map findWordMatches(String str, List<Map<String,Object>> list) {
//         Map<String,Object> mapResult = new HashMap();
//         mapResult.put("hasText",false);
//         for(Map<String,Object> mapData : list){
//             String text = String.valueOf(mapData.get("inputText"));
//             int hasText = str.indexOf(text);
//             if(hasText != -1){
//                 mapResult.put("hasText",true);
//                 mapResult.put("isText",mapData.get("outputText"));
//                 break;
//             }
//         }
//         return mapResult;
//     }

//     @Override
//     public String seperateNameGroupLine(String input) {

//         String wordSeperate = "";
//         MasterData masterData = masterDataRepository.findByCode(ConstantVariable.CODE_USE_FOR_SEPERATE_WORD);
//         for(MasterDataDetail dataDetail : masterData.getDetails()){
//             wordSeperate += ","+dataDetail.getVariable1();
//         }
//         wordSeperate = !"".equals(wordSeperate) ?  wordSeperate.substring(1) : wordSeperate;
//         String[] wordListSplit = wordSeperate.split(",");
//         String realDisplayName = null;
//         for(String data : wordListSplit){
//             if(input.indexOf(data) != -1){
//                 System.out.println(data + " containt in "+input);
//                 realDisplayName = (input.split(data)[1]).trim();
//                 break;
//             }

//         }

//         if(realDisplayName != null)
//             return realDisplayName;
//         else
//             return null;
//     }

// 	@Override
// 	public Map<String,Object> findMessageResponse(String str,String groupId) {
// 		// TODO Auto-generated method stub
		
// 		Map<String,Object> map = new HashMap();
		
// 		List<Priority> priorityList = priorityRepository.findAll();
//         for(Priority data : priorityList){
        	
//             if(str.trim().startsWith(data.getHasText()) && 
//             		"Y".equals(data.getFlagActive()) && 
//             		"1".equals(String.valueOf(data.getValue())) &&
//             		groupId.equals(data.getGroupLineRealId())){
//             	map.put("TYPE", data.getResponseType());
//         		map.put("VALUE", data.getResponseValue());
//         		break;

//             }

//         }
// 		return map;
// 	}
// }
