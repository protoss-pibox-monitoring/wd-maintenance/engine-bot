package com.protoss.pm.bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.protoss.pm.bot.entity.ProjectGroup;

public interface ProjectGroupRepository extends JpaSpecificationExecutor<ProjectGroup>,
        JpaRepository<ProjectGroup, Long>,
        PagingAndSortingRepository<ProjectGroup, Long> {

    ProjectGroup findFirstByRealId(@Param("realId")String realId);


}
