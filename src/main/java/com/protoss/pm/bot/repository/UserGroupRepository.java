package com.protoss.pm.bot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.protoss.pm.bot.entity.UserGroup;

public interface UserGroupRepository extends JpaSpecificationExecutor<UserGroup>,
        JpaRepository<UserGroup, Long>,
        PagingAndSortingRepository<UserGroup, Long> {



	List<UserGroup> findByGroupIdAndUserIdAndRoleAccess(@Param("groupId")String groupId,@Param("userId")String userId,@Param("roleAccess")String roleAccess);
	UserGroup findFirstByGroupIdAndUserId(@Param("groupId")String groupId,@Param("userId")String userId);


}
