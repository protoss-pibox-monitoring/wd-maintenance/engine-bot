package com.protoss.pm.bot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.protoss.pm.bot.entity.LineLogMessage;
import com.protoss.pm.bot.entity.MasterData;

public interface LineLogMessageRepository extends JpaSpecificationExecutor<LineLogMessage>,
        JpaRepository<LineLogMessage, Long>,
        PagingAndSortingRepository<LineLogMessage, Long> {


}
