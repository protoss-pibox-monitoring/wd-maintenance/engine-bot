package com.protoss.pm.bot.controller;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.LeaveEvent;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.*;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import com.protoss.pm.bot.entity.Cards;
import com.protoss.pm.bot.entity.GroupLine;
import com.protoss.pm.bot.entity.IssueMessageWord;
import com.protoss.pm.bot.entity.LineHistoryLogMessage;
import com.protoss.pm.bot.entity.LineLogMessage;
import com.protoss.pm.bot.entity.Priority;
import com.protoss.pm.bot.entity.UserGroup;
import com.protoss.pm.bot.entity.UserLine;
import com.protoss.pm.bot.repository.IssueMessageWordRepository;
import com.protoss.pm.bot.repository.PriorityRepository;
import com.protoss.pm.bot.service.AppLineBotDataService;
import com.protoss.pm.bot.service.CardsService;
import com.protoss.pm.bot.service.GroupLineService;
import com.protoss.pm.bot.service.LineHistoryLogMessageService;
import com.protoss.pm.bot.service.LineLogMessageService;
import com.protoss.pm.bot.service.NetPieService;
import com.protoss.pm.bot.service.UserGroupService;
import com.protoss.pm.bot.service.UserLineService;
import com.protoss.pm.bot.util.ConstantVariable;
import com.protoss.pm.bot.util.CrcUtil;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Slf4j
@LineMessageHandler
public class LineBotController {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Autowired
    private AppLineBotDataService appLineBotDataService;

    @Autowired
    private LineLogMessageService lineLogMessageService;

    @Autowired
    private LineHistoryLogMessageService lineHistoryLogMessageService;
    
    @Autowired
    private GroupLineService groupLineService;
    
    @Autowired
    private UserLineService userLineService;
    
    @Autowired
    private UserGroupService userGroupService;
    
    @Autowired
    private CardsService cardsService;
    
    @Autowired
    private NetPieService netPieService;

    public static List<Map<String,Object>> LIST_KEYWORD = new ArrayList<>();
    public static String realDisplayName = "";



    @EventMapping
    public void handleTextMessage(MessageEvent<TextMessageContent> event) throws IOException {

        handleTextContent(event.getReplyToken(), event, event.getMessage());

    }

    private void replyText(@NonNull  String replyToken, @NonNull String message) {
        if(replyToken.isEmpty()) {
            throw new IllegalArgumentException("replyToken is not empty");
        }

        if(message.length() > 1000) {
            message = message.substring(0, 1000 - 2) + "...";
        }
        this.reply(replyToken, new TextMessage(message));
    }
    
    private void replyImage(@NonNull  String replyToken, @NonNull String message) {
        if(replyToken.isEmpty()) {
            throw new IllegalArgumentException("replyToken is not empty");
        }
        this.reply(replyToken, new ImageMessage(message, message));
    }

    private void reply(@NonNull String replyToken, @NonNull Message message) {
        reply(replyToken, Collections.singletonList(message));
    }

    private void reply(@NonNull String replyToken, @NonNull List<Message> messages) {
        try {
            BotApiResponse response = lineMessagingClient.replyMessage(
                    new ReplyMessage(replyToken, messages)
            ).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }

    }


    private void handleTextContent(String replyToken, Event event, TextMessageContent content) throws IOException{
            log.info(" ===- In[handleTextContent] Msg : [{}] -===",content.getText());

            try{
               
                String text = (content.getText().trim()).toUpperCase();
                log.info(" ===- Text Cleansing : [{}]  -===",text);
                // netPieService.chat("WDP1", text);
                 
               
                
                        log.info(" ===- switch(Profile)  -===");
                         String userId = event.getSource().getUserId();
                        //  if(userId != null) {
                        //       lineMessagingClient.getProfile(userId)
                        //       .whenComplete((profile, throwable) -> {
                        //         if(throwable != null) {
                        //            log.info(" ===- throwable -===");
                        //             return;
                        //         }
                        //         log.info(" ===- found User -===");
                                

                        //          if(!text.startsWith("#") && !text.equals(ConstantVariable.WEBHOOK_FIRST_RESPONSE_TEXT)){
                        //             String[] textVale = text.split(" ");
                        //            if(textVale.length==2){
                        //             String gearName = textVale[0].trim();
                        //             String message = textVale[1].trim();
                        //             message = "Sender : "+profile.getDisplayName()+", Message : "+message;
                        //             log.info(" ===- gearName [{}] -===",gearName);
                        //             log.info(" ===- message [{}] -===",message);
                        //             netPieService.chat(gearName, message);
                        //             log.info(" ===- Sending to WDP1 Success :  -===");
                        //        }
                        //      } 
                        
                        //     });
                        //  }


                        if(!text.startsWith("#") && !text.equals(ConstantVariable.WEBHOOK_FIRST_RESPONSE_TEXT)){
                            if("STOP".equals(text) || "CHECK".equals(text)){
                                netPieService.chat("WDP3", text);
                            }
                    //         String[] textVale = text.split(" ");
                    //        if(textVale.length==2){
                    //         String gearName = textVale[0].trim();
                    //         String message = textVale[1].trim();
                    //         // message = "Sender : "+profile.getDisplayName()+", Message : "+message;
                    //         log.info(" ===- gearName [{}] -===",gearName);
                    //         log.info(" ===- message [{}] -===",message);
                    //         netPieService.chat(gearName, message);
                    //         log.info(" ===- Sending to WDP1 Success :  -===");
                    //    }
                     } 
                


                   
                    // }
                    


                // }
                
                log.info(" ===- Out[handleTextContent] Msg : [{}] -===");
            }
            catch(Exception e){
                log.error(" ===- Exception[handleTextContent] Msg : [{}] -===",e.getMessage());
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
            }

    }





}
