package com.protoss.pm.bot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.protoss.pm.bot.entity.MasterDataDetail;
import com.protoss.pm.bot.service.AppLineBotDataService;
import com.protoss.pm.bot.util.ConstantVariable;

import java.util.List;

@RestController
@RequestMapping("/alarm")
public class AlarmController {

    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());


    @GetMapping("/getStatus")
    public String getStatusAlarm(){
        return ConstantVariable.ALARM_ACTIVE;
    }

    @PostMapping("/enableAlarm")
    public String enableAlarm(){
        ConstantVariable.ALARM_ACTIVE = "1";
        return "1";
    }

    @PostMapping("/disableAlarm")
    public String disableAlarm(){
        ConstantVariable.ALARM_ACTIVE = "0";
        return "1";
    }

}
